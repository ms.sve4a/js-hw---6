function forEach(array, callback){
        if(!Array.isArray(array)){
            return console.log("First argument must be an array");
        }
        if(typeof callback !== 'function') {
            return console.log("Second argument must be a function");
        }

        for(let i = 0; i < array.length; i++) {
            let item = array[i];
            callback(item, i);
        }

    }

    forEach([1, 2, 3, 4, 5, 6, 7], (item, index) => {
        if(typeof item === "string") {
            console.log(item, index);
        }
    });
    