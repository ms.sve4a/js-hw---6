
function filterBy(array, type) {
    return array.filter(function (item) {

        if (typeof item !== type ) {
            return true;
        }
        return false;
    });

}

filterBy([[], {}, 'svkg', 23, 56, ''], 'string');


/*

function filterBy2(array, type) {
    return array.filter(item => typeof item !== type);
}*/
